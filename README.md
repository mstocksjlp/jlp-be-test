# JLP Back-end Technical Test - Dresses

## Brief

The code within this repo simplifies a JLP product endpoint that returns dresses.  We would like you to add a new restful endpoint that only returns dresses reduced in price.
The new endpoint also needs the json response to be modified to match the following:
An array of products. Each element should contain:
- **productId** String
- **title** String
- An array of **colorSwatches**. Each element should contain:
  - **color** String
  - **rgbColor** String which is an RGB  representation of the basicColor in a six digit hexadecimal format, e.g. “F0A1C2”.
  - **skuid** String
- **nowPrice** String which is the price.now represented as a string, including the currency, e.g. “£1.75”. For values that are integer, if they are less £10 return a decimal price, otherwise show an integer price, e.g. “£2.00” or  “£10”.
- **priceLabel** String. An optional query parm called labelType can be set to any of:
  - ShowWasNow - in which case return a string saying “Was £x.xx, now £y.yyy”.
  - ShowWasThenNow - in which case return a string saying “Was £x.xx, then £y.yy, now £z.zzz”. If there is no value in price.then just return was and now as above.
  - ShowPercDscount  - in which case return “x% off - now £y.yy”.
  
If the query parm is not set default to use ShowWasNow format.
In all cases use the price formatting as described for nowPrice.

### Looking up RGB values

Create a hash table or similar device to translate basicColor to RGB.

### Trapping errors with the data provided by the API
If there is invalid data on the input file then write out an empty string or assume a value of zero rather than throwing exceptions or creating complex error logging.


## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Junit setup.
- Put all your assumptions, notes, instructions and improvement suggestions into your README.md.
- We'll be assessing your coding style and how you've approached this task.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- To build the project run `/gradlew clean build`
- Add your API key from your invitation email to the `jl.api.key` property  
- Run the springboot app using `./gradlew bootRun` or through the IDE and open [http://localhost:8080/products/dresses](http://localhost:8080/products/dresses) in your browser.
- View the API documentation via [http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/) in your browser
