package com.johnlewis.betechtest.products.controller;

import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.service.DressesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class ProductsControllerTest {

    private final DressesService dressesService = mock(DressesService.class);
    private final ProductsController productsController = new ProductsController(dressesService);

    @AfterEach
    void after(){
        verifyNoMoreInteractions(dressesService);
    }

    @Test
    void shouldTest(){
        ResponseEntity<List<Product>> result = productsController.getDiscountDresses();
        verify(dressesService).getDresses();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}