package com.johnlewis.betechtest.products.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * For a product that is not on sale it will just have a now value.
 * For a product that has been reduced once it will have a was and now value.
 * For a prodcut that has been reduced more than once it will have all values.
 */
@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Price {
    private String was;
    private String then;
    private String now;
}
