package com.johnlewis.betechtest.products.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class Product {
    private String productId;
    private String title;
    private Price price;
    private List<ColorSwatch> colorSwatches;
}
